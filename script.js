const BoxBatu = document.getElementById('playerbatu');
const BoxKertas = document.getElementById('playerkertas');
const BoxGunting = document.getElementById('playergunting');
const refresh = document.getElementById('refresh');
const BoxBatuCom = document.getElementById('combatu');
const BoxKertasCom = document.getElementById('comkertas');
const BoxGuntingCom = document.getElementById('comgunting');
const PlayerWin = document.getElementById('pwin');
const Draw = document.getElementById('draw');
const ComWin = document.getElementById('comwin');
const VS = document.getElementById('result');

let pilihanUser = "";
let pilihanComp = "";

const result = () => {
    if(
        (pilihanUser === "batu" && pilihanComp === "combatu") ||
        (pilihanUser === "kertas" && pilihanComp === "comkertas") ||
        (pilihanUser === "gunting" && pilihanComp === "comgunting")
    ){
        console.log('DRAW');
        VS.style.display = "none";
        Draw.style.display = "";
    } else if (pilihanUser === "batu" && pilihanComp === "comgunting"){
        console.log('Player Win');
        VS.style.display = "none";
        PlayerWin.style.display = "";
    } else if (pilihanUser === "kertas" && pilihanComp === "combatu"){
        console.log('Player Win');
        VS.style.display = "none";
        PlayerWin.style.display = "";
    } else if (pilihanUser === "gunting" && pilihanComp === "comkertas"){
        console.log('Player Win');
        VS.style.display = "none";
        PlayerWin.style.display = "";
    } else {
        console.log('COM Win');
        VS.style.display = "none";
        ComWin.style.display = "";
    }
}




class Computer {

    backgroundcom = (pilih) => {
        const selectedBox = document.getElementById(pilih);
        selectedBox.style.backgroundColor = '#C4C4C4';
    };
    pilihanCom = () => {
        const pilihanComputer = ['combatu', 'comkertas', 'comgunting'];
        const rnInt = Math.floor(Math.random() * 3);
        const pilihan = pilihanComputer[rnInt];
        pilihanComp = pilihanComputer[rnInt];
        this.backgroundcom(pilihan);
        console.log(pilihanComp);
        result();
    };
}

const Komputer =new Computer();

BoxBatu.onclick = () => {
    BoxBatu.style.backgroundColor = '#C4C4C4';
    pilihanUser = "batu";
    disableButton();
    console.log(pilihanUser);
    Komputer.pilihanCom();
}

BoxKertas.onclick = () => {
    BoxKertas.style.backgroundColor = '#C4C4C4';
    pilihanUser = "kertas";
    disableButton();
    console.log(pilihanUser);
    Komputer.pilihanCom();
}

BoxGunting.onclick = () => {
    BoxGunting.style.backgroundColor = '#C4C4C4';
    pilihanUser = "gunting";
    disableButton();
    console.log(pilihanUser);
    Komputer.pilihanCom();
}

const disableButton = () => {
    BoxBatu.style.pointerEvents = 'none';
    BoxGunting.style.pointerEvents = 'none';
    BoxKertas.style.pointerEvents = 'none';
}

refresh.onclick = () => {
    BoxBatu.style.backgroundColor = '#9C835F'
    BoxKertas.style.backgroundColor = '#9C835F'
    BoxGunting.style.backgroundColor = '#9C835F'
    BoxBatuCom.style.backgroundColor = '#9C835F'
    BoxKertasCom.style.backgroundColor = '#9C835F'
    BoxGuntingCom.style.backgroundColor = '#9C835F'
    BoxBatu.style.pointerEvents = '';
    BoxGunting.style.pointerEvents = '';
    BoxKertas.style.pointerEvents = '';
    ComWin.style.display = "none";
    PlayerWin.style.display = "none";
    Draw.style.display = "none";
    VS.style.display = "";
    console.log('Mulai Permainan');
}

